#include <stdint.h>
#include <stdio.h>

#include "transport.h"

static struct t_tran_ctx g_ctx = { .hCom = INVALID_HANDLE_VALUE, };

static int trans_com_speed( struct t_tran_ctx *ctx, long rate )
{
	DCB dcb;

	PurgeComm( ctx->hCom, (PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR) );

	dcb.DCBlength = sizeof(DCB);
	dcb.BaudRate = rate;
	dcb.fBinary = TRUE;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;


	dcb.fOutxCtsFlow = FALSE;
	dcb.fOutxDsrFlow = FALSE;

    dcb.fDtrControl = DTR_CONTROL_ENABLE;
    dcb.fRtsControl = RTS_CONTROL_ENABLE;

	dcb.fDsrSensitivity = FALSE;
	dcb.fTXContinueOnXoff = FALSE;
	dcb.fOutX = FALSE;
	dcb.fInX = FALSE;
	dcb.fErrorChar = FALSE;
	dcb.fNull = FALSE;
	dcb.fAbortOnError = FALSE;
	dcb.XonLim = 32768;
	dcb.XoffLim = 8192;

	dcb.XonChar = 0;
	dcb.XoffChar = 0;
	dcb.ErrorChar = 0;
	dcb.EofChar = 0;
	dcb.EvtChar = 0;
	dcb.wReserved = 0;


	return SetCommState( ctx->hCom, &dcb);
}


struct t_tran_ctx *trans_init( int ncom )
{
	HANDLE hPort;
	char pcName[16];

	sprintf( pcName, "COM%d:", ncom );

	hPort = CreateFile( pcName, GENERIC_READ | GENERIC_WRITE, 0, 0,
		OPEN_EXISTING, 0, 0);
	
    if( hPort == INVALID_HANDLE_VALUE )
        return 0;

    g_ctx.hCom = hPort;
    g_ctx.ncom = ncom;

    if( trans_com_speed( &g_ctx, 115200 ) == 0 )
        return 0;

    //PurgeComm( g_ctx.hCom,  (PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR) );
    //if( ClearCommError( g_ctx.hCom, 0, 0) == 0 )
    //    return 0;
    return &g_ctx;
}

int trans_deinit( struct t_tran_ctx *ctx )
{
    if( !ctx )
        return -1;
    CloseHandle( ctx->hCom );
    ctx->hCom = INVALID_HANDLE_VALUE;
    return 0;
}

/* not working with some USB-TTL chips ( bad drivers ) */
int trans_data_inqueue( struct t_tran_ctx *ctx )
{
    COMMPROP cp;
    if( !ctx )
        return -1;

    if( GetCommProperties( ctx->hCom, &cp ) )
    {
        return cp.dwCurrentRxQueue;
    }
    return 0;
}

int trans_data_write_buffer( struct t_tran_ctx *ctx, uint8_t *b, uint8_t size )
{
	DWORD dwLen;

    if( !ctx )
        return -1;

    if(!WriteFile( ctx->hCom, b,size, &dwLen, NULL))
        return -1;
    
    if( dwLen != size )
        return -1;

    return dwLen;
}

int trans_data_read_buffer( struct t_tran_ctx *ctx, uint8_t *b, uint8_t size, uint32_t t )
{
	COMMTIMEOUTS sTimeouts;
	DWORD dwLen;

    if( !ctx )
        return -1;

	sTimeouts.ReadIntervalTimeout = 0;
	sTimeouts.ReadTotalTimeoutMultiplier = 0;
	sTimeouts.ReadTotalTimeoutConstant = t;
	sTimeouts.WriteTotalTimeoutMultiplier = 0;
	sTimeouts.WriteTotalTimeoutConstant = 0;
	
	SetCommTimeouts( ctx->hCom, &sTimeouts );

	if(!ReadFile( ctx->hCom, b, size, &dwLen, NULL))
        return -1;

	return dwLen;
}


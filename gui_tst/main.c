#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <tchar.h>
#include "main.h"

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>

#include "wl_tools.h"
#include "transport.h"

BOOL CALLBACK MainDlg_DlgProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
void MainDlg_OnClose(HWND hwnd);
void MainDlg_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
BOOL MainDlg_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
void MainDlg_OnTimer(HWND hwnd, UINT id);


HANDLE ghInstance;
HWND hEdit;
HWND hWin;
HWND hTrack;
struct t_tran_ctx *g_comm = 0;


int printf_d( char *fmt, ... )
{
    char buf[0x100];
    int len;

    va_list va;
    va_start( va, fmt);
    vsprintf( buf, fmt, va );
    va_end(va);


    len = GetWindowTextLength( hEdit );
    if( len > 16*1024 )
    {
        SetWindowText( hEdit, "" );
        len = GetWindowTextLength( hEdit );
    }
    SetFocus ( hEdit );
    SendMessageA( hEdit, EM_SETSEL, (WPARAM)len, (LPARAM)len);
    SendMessageA( hEdit, EM_REPLACESEL, 0, (LPARAM)buf );
    return 0;
}

int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
    INITCOMMONCONTROLSEX icc;
    WNDCLASSEX wcx;

    ghInstance = hInstance;

    icc.dwSize = sizeof(icc);
    icc.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&icc);

    wcx.cbSize = sizeof(wcx);
    if (!GetClassInfoEx(NULL, MAKEINTRESOURCE(32770), &wcx))
        return 0;

    wcx.hInstance = hInstance;
    wcx.hIcon = NULL;
    wcx.lpszClassName = _T("gui_tstClass");
    if (!RegisterClassEx(&wcx))
        return 0;

    return DialogBox(hInstance, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)MainDlg_DlgProc);
}


BOOL CALLBACK MainDlg_DlgProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	HANDLE_MSG (hwnd, WM_CLOSE, MainDlg_OnClose);
	HANDLE_MSG (hwnd, WM_COMMAND, MainDlg_OnCommand);
	HANDLE_MSG (hwnd, WM_INITDIALOG, MainDlg_OnInitDialog);
	HANDLE_MSG (hwnd, WM_TIMER, MainDlg_OnTimer);
	default: return FALSE;
	}
}


void MainDlg_OnClose(HWND hwnd)
{
    trans_deinit( g_comm );
    EndDialog( hwnd, 0 );
}


void MainDlg_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
    char tmp[0x100];
    int  t;

    switch (id)
    {
        case ID_SEND_AS_DEVICE:
        { 
            t = GetDlgItemInt(hwnd, IDC_DEV_SERIAL, 0, 0);
            GetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), tmp, sizeof(tmp));
            t = packet_create_ex(t, tmp);
            if (t > 0)
            {
                SetWindowText(GetDlgItem(hwnd, IDC_OUTPUT_DATA), tmp);
            }
            break;
        }
        case ID_CLEAR_LOG:
        {
            SetWindowText( hEdit, "" );
            break;
        }
        case ID_SEND_DATA:
        {

            t = GetDlgItemInt(hwnd, IDC_DEV_SERIAL, 0, 0);
            GetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), tmp, sizeof(tmp));
            /* will return new buffer to tmp */
            t = packet_create(t, tmp);
            if (t > 0)
            {
                SetWindowText(GetDlgItem(hwnd, IDC_OUTPUT_DATA), tmp);
                if( trans_data_write_buffer( g_comm, (void*)tmp, t ) != t )
                {
                    printf_d( "write data error for size: %d\r\n", t );
                }
            }
            break;
        }
        case ID_VOICE_CALL:
        {
            char phone[15] = { 0 };
            GetWindowText(GetDlgItem(hwnd, IDC_PHONE), phone, sizeof(phone));
            t = voice_call_cmd( phone, tmp );
            if( t > 0 )
            {
                SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), tmp );
            }
            break;
        }
        case ID_RESET_CMD:
        {
            SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), "00 02");
            break;
        }
        case ID_CLEAR_EVENTS:
        {
            SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), "07 00 00 80");
            break;
        }
        case ID_REQUEST_STATUS:
        {
            SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), "00");
            break;
        }
        case ID_REQUEST_IMEI:
        {
            SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), "1D");
            break;
        }
        case ID_TRACKING:
        {
            int index = ComboBox_GetCurSel( hTrack );
            if( index == -1 )
                break;
            sprintf( tmp, "0B %.2X 00", index );
            SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), tmp );
            break;
        }
        case ID_SYNC_TIME:
        {
            uint32_t t = time( 0 );
            /* -37 years */
            t -= 0x45984F00;
            sprintf( tmp, "01 %.2X %.2X %.2X %.2X 0C", 
                    (t&0xFF), (t>>8)&0xFF, (t>>16)&0xFF, (t>>24)&0xFF );
            SetWindowText(GetDlgItem(hwnd, IDC_INPUT_DATA), tmp );
            break;
        }
        case ID_OPEN_UART:
        {
            trans_deinit( g_comm );
            g_comm = trans_init(GetDlgItemInt(hwnd, IDC_COMM_NUM, 0, 0));
            printf_d( "Serial port: %s\r\n", g_comm? "OK":"ERROR" );
            
            if( g_comm )
            {
                KillTimer(hwnd, 666);
                SetTimer(hwnd, 666, 250, 0);
            }
            break;
        }
        case ID_SEND_RAW:
        {
            GetWindowText(GetDlgItem(hwnd, IDC_RAW_DATA), tmp, sizeof(tmp));
            strcat( tmp, "\r\n" );
            trans_data_write_buffer( g_comm, (void*)tmp, strlen( tmp ) );
            break;
        }
        case ID_INIT_COMM:
        {
            trans_data_write_buffer( g_comm, (void*)"\\tdi\r\n", 6 );
            Sleep( 500 );
            trans_data_write_buffer( g_comm, (void*)"\\tf1\r\n", 6 );
            break;
        }
        case ID_GPS_TEST:
        {
            trans_data_write_buffer( g_comm, (void*)"\\tg1\r\n", 6 );
            Sleep( 800 );
            trans_data_write_buffer( g_comm, (void*)"\\tdg\r\n", 6 );
            break;
        }
        case ID_MODEM_TEST:
        {
            trans_data_write_buffer( g_comm, (void*)"\\tdm\r\n", 6 );
            Sleep( 800 );
            trans_data_write_buffer( g_comm, (void*)"\\tr\r\n", 5 );
            break;
        }
        case ID_NORMAL_MODE:
        {
            trans_data_write_buffer( g_comm, (void*)"\\td\r\n", 5 );
            Sleep( 500 );
            trans_data_write_buffer( g_comm, (void*)"\\tf0\r\n", 6 );
            break;
        }
    }
}

static char *track_timeouts[] =
{
    "None", 
    "10 sec", "15 sec", "20 sec", "25 sec", "30 sec", "40 sec", "50 sec",
    "1 min", "1.5 min", "2 min", "2.5 min", "3 min", "4 min", "5 min", 
    "6 min", "7 min", "8 min", "9 min", "10 min", "15 min", "20 min",
    "25 min", "30 min", "35 min", "40 min", "45 min", "50 min", "60 min",
    "70 min", "80 min", "90 min", "100 min", "110 min", "2 hours",
    "2.5 hours", "3 hours", "3.5 hours", "4 hours", "4.5 hours", "5 hours",
    "6 hours", "7 hours", "8 hours", "9 hours", "10 hours", "12 hours",
    "14 hours", "16 hours", "18 hours", "20 hours", "1 days", "1.5 days",
    "2 days", "2.5 days", "3 days", "3.5 days", "4 days", "4.5 days", "5 days", 0
};

BOOL MainDlg_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
    int i;

    hEdit = GetDlgItem( hwnd, IDC_LOG_WIN );
    hTrack =  GetDlgItem( hwnd, IDC_TRACK_TIME );
    hWin = hwnd;

    for( i = 0; track_timeouts[i]; i++ )
    {
        ComboBox_AddString( hTrack, track_timeouts[i] );
    }
    ComboBox_SetCurSel( hTrack, 0 );

    printf_d( "System started\r\n" );
    return TRUE;
}

void MainDlg_OnTimer(HWND hwnd, UINT id)
{
    static uint16_t index = 0;
    static char buff[0x400];
    char ch;
    int result;

    if( id != 666 )
        return;

    do
    {
        result = trans_data_read_buffer( g_comm, (void*)&ch, 1, 1 );
        if( result <= 0 )
            continue;

        buff[index++] = ch;
        if( ch != '\n' )
            continue;

        buff[index] = 0;
        printf_d( buff );
        if( buff[0] == '<' && (index > 1) )
        {
            result = packet_process( &buff[1] );
            if( result > 0 )
            {
                printf_d( "rx: %s\r\n", &buff[1] );
            }
        }
        index = 0;
    }
    while( result > 0 );
}


/* gui render function */
void print_control( uint32_t ctrl, char *fmt, ... )
{
    char buf[0x100];
    int control_id;

    va_list va;
    va_start( va, fmt);
    vsprintf( buf, fmt, va );
    va_end(va);

    switch( ctrl )
    {
        case IMEI_CONTROL:
            control_id = IDC_MODEM_IMEI;
        break; 
        case SERIAL_CONTROL:
            control_id = IDC_UID_SERIAL;
        break;
        case HARDWARE_CONTROL:
            control_id = IDC_HARDWARE_DESC;
        break;
        case UNIT_TIME_CONTROL:
            control_id = IDC_UNIT_TIME;
        break;
        case MILEAGE_CONTROL:
            control_id = IDC_MILEAGE_KM;
        break;
        case MODEM_SIGNAL_CONTROL:
            control_id = IDC_GSM_LEVEL;
        break;
        case FW_VER_CONTROL:
            control_id = IDC_FW_VERSION;
        break;
        case BATTERY_VOLTAGE:
            control_id = IDC_BATTERY;
        break;
        case STATE_CONTROL:
            control_id = IDC_DEV_STATE;
        break;
        case LONG_CONTROL:
            control_id = IDC_GPS_LONG;
        break;
        case LAT_CONTROL:
            control_id = IDC_GPS_LAT;
        break;
        case SPEED_CONTROL:
            control_id = IDC_GPS_SPEED;
        break;
        case ANGLE_CONTROL:
            control_id = IDC_GPS_ANGLE;
        break;
        case ALT_CONTROL:
            control_id = IDC_GPS_ALT;
        break;
        case SATS_CONTROL:
            control_id = IDC_GPS_SATS;
        break;
        case GPS_VALID_CONTROL:
            control_id = IDC_GPS_VALID;
        break;
        case GPS_POWER_CONTROL:
            control_id = IDC_GPS_POWER;
        break;
        case GPS_TIME_CONTROL:
            control_id = IDC_GPS_TIME;
        break;
        case GPS_HW_TYPE:
            control_id = IDC_GPS_HW_TYPE;
        break;
        case GPS_HDOP_CONTROL:
            control_id = IDC_GPS_HDOP;
        break;
        case GPS_TTFF_CONTROL:
            control_id = IDC_GPS_TTFF;
        break;
        case DEV_REASON_CONTROL:
            control_id = IDC_DEV_REASON;
        break;
        case INSTALL_CONTROL:
            control_id = IDC_INSTALL_STATE;
        break;
        case STATIC_PIN_CONTROL:
            control_id = IDC_STAT_PIN;
        break;
        case MOBILE_PIN_CONTROL:
            control_id = IDC_MOB_PIN;
        break;
        case TIMEOUT_CONTROL:
            control_id = IDC_COM_TIMEOUT;
        break;
        default:
            return;  
    }
     
    SetWindowText(GetDlgItem( hWin, control_id ), buf );
}


#pragma once

#include <time.h>

#pragma pack(push,1)
struct t_command
{
    char        start;
    uint32_t    serial;
    uint8_t     rnd;
    uint8_t     data[8];
    uint8_t     xor8;
};
#pragma pack(pop)

struct t_packet_ctx
{
    uint8_t     *b;
    uint8_t     size;
    uint8_t     protocol_id;
    uint8_t     packet_type;
    uint32_t    com_timeout;
    uint16_t    fw_version;
};

int prepare_packet( uint8_t decrypt, uint8_t *in, uint8_t *out, uint8_t size );
int packet_create( uint32_t serial, char *in );
int packet_create_ex( uint32_t serial, char *in );
int packet_process( char *s );

int voice_call_cmd( char *phone, char *out );
int hexstring_to_bin( char *s, uint8_t *out, uint8_t size );
void print_control( uint32_t ctrl, char *fmt, ... );


#define IMEI_CONTROL            1
#define SERIAL_CONTROL          2    
#define HARDWARE_CONTROL        3
#define UNIT_TIME_CONTROL       4
#define MILEAGE_CONTROL         5
#define MODEM_SIGNAL_CONTROL    6
#define FW_VER_CONTROL          7
#define BATTERY_VOLTAGE         8
#define STATE_CONTROL           9
#define LONG_CONTROL            10
#define LAT_CONTROL             11
#define SPEED_CONTROL           12
#define ANGLE_CONTROL           13
#define ALT_CONTROL             14
#define SATS_CONTROL            15
#define GPS_VALID_CONTROL       16
#define GPS_POWER_CONTROL       17
#define GPS_TIME_CONTROL        18
#define GPS_HW_TYPE             19
#define GPS_HDOP_CONTROL        20
#define GPS_TTFF_CONTROL        21
#define DEV_REASON_CONTROL      22
#define INSTALL_CONTROL         23
#define STATIC_PIN_CONTROL      24
#define MOBILE_PIN_CONTROL      25
#define TIMEOUT_CONTROL         36

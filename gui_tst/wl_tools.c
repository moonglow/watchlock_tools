#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include "wl_tools.h"

/* WL use modified XXTEA encription, 32 rounds */

static uint32_t xxtea_key[4] = 
{
    0x4A1CB24E,
    0x068EC892,
    0xD9589FB6,
    0xF4988430
};

#define MX  ( ((z >> 5 ^ z << 2) + ((z >> 3 ^ z << 4))) ^ ((sum ^ z) + (k[(p & 3) ^ e] ^ z)) )

static void xxtea_decrypt_block( uint32_t *v, uint32_t *k )
{
    uint32_t z, y = v[0], sum = 0, e, DELTA = 0x9e3779b9;
    uint32_t p, q;

    q = 32;
    sum = q * DELTA;
    while (sum != 0)
    {
        e = sum >> 2 & 3;
        
        p = 1;
        z = v[0];
        y = v[1] -= MX;

        p = 0;
        z = v[1];
        y = v[0] -= MX;
        sum -= DELTA;
    }   
}

static void xxtea_encrypt_block( uint32_t *v, uint32_t *k )
{
    uint32_t z, y = v[0], sum = 0, e, DELTA = 0x9e3779b9;
    uint32_t p, q;

    z = v[1];
    q = 32;
    while (q-- > 0)
    {
        sum += DELTA;
        e = sum >> 2 & 3;

        p = 0;
        y = v[1];
        z = v[0] += MX;

        p = 1;
        y = v[0];
        z = v[1] += MX;
    }
}

static uint8_t calc_xor8( uint8_t *b, uint8_t size )
{
    uint8_t crc = 0;

    while( size-- )
    {
        crc ^= b[size];
    }
    return crc;
}

/* string helpers */
static uint8_t xstrlen( char *s )
{
    uint8_t t = 0;
    while( *s )
    {
        ++s;
        ++t;
    }
    return t;
}

/* protocol parser */
static uint8_t wl_crypted( struct t_packet_ctx *ctx )
{
    /* data crypted ?*/
    if( (ctx->b[5] & 0x40) )
        return 1;
    return 0;
}

static uint8_t wl_offset( struct t_packet_ctx *ctx )
{
    /* need to shift offset ?*/
    if( (ctx->b[5] & 0x20) )
        return 1;
    return 0;
}

static uint32_t wl_get_device_serial( struct t_packet_ctx *ctx ) 
{
    return ( (ctx->b[4]<<24)|(ctx->b[3]<<16)|(ctx->b[2]<<8)|(ctx->b[1]<<0) );   
}

/* get device hardware model */
uint8_t wl_get_hw_model( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx ) + 0x06;
    if( offset >= ctx->size )
        return 0;  
    
    return ctx->b[offset-1];
}

static int wl_get_modem_imei( struct t_packet_ctx *ctx, char *out )
{
    uint8_t offset, len;
    offset = wl_offset( ctx ) + 0x07;
    if( offset >= ctx->size )
        return 0; 
   
    len = ctx->b[offset-1];
    
    offset = wl_offset( ctx ) + 0x08;
    
    out[len] = 0;
    while( len-- )
    {
        out[len] = ctx->b[offset-1+len];
    }
    return xstrlen( out );
}

time_t wl_get_unit_time( struct t_packet_ctx *ctx )
{
    time_t rawtime;
    uint8_t  offset;

    offset = wl_offset( ctx ) + 0x0A;
    if( offset > ctx->size )
        return 0;

    offset = wl_offset( ctx ) + 7;
    rawtime = *((time_t*)&ctx->b[offset-1]);

    return rawtime;
}

uint8_t wl_get_gps_type( struct t_packet_ctx *ctx )
{
    uint8_t  offset;

    offset = wl_offset( ctx ) + 0x18;
    if( offset > ctx->size )
        return 0xFF;
    
    return ctx->b[offset-1];
}

char *wl_get_gps_type_s( struct t_packet_ctx *ctx )
{
    switch ( wl_get_gps_type( ctx ) )
    {
        case 1: return "uBlox4";
        case 2: return "uBlox6";
        case 3: return "Origin";
        case 4: return "OriginB";
        case 5: return "SkyTraq";
        case 6: return "Telit";
        case 7: return "Quectel";
        case 8: return "uBlox8";
        default: return "Unknown (";
    }    
}

char *wl_get_device_type_s( struct t_packet_ctx *ctx ) 
{
    switch( wl_get_hw_model( ctx ) )
    {
        case 0x01: return "Rainbow";
        case 0x02: return "Helios Mot24";
        case 0x03: return "Helios MotUSB";       
        case 0x04: return "Helios Enf24";        
        case 0x05: return "Helios EnfUSB";     
        case 0x06: return "Triton Enf";  
        case 0x07: return "Watchlock Mot";      
        case 0x08: return "Triton Mot";
        case 0x09: return "Helios Tel24";
        case 0x0A: return "Helios Tel10";       
        case 0x0B: return "Helios uBx24";        
        case 0x0C: return "Helios uBx10";        
        case 0x0D: return "Watchlock uBx";        
        case 0x0E: return "Triton uBx";       
        case 0x0F: return "Rainbow uBx";       
        case 0x10: return "Helios Mot24b";      
        case 0x11: return "Helios Mot10";      
        case 0x12: return "Kylos 2 Tel";     
        case 0x13: return "Triton 2 Tel";     
        case 0x14: return "Helios TTa";     
        case 0x15: return "Helios TT";   
        default: return "Unknown (";
    }
}

uint32_t wl_get_gps_mileage( struct t_packet_ctx *ctx )
{
    uint32_t raw;
    uint8_t  offset;

    offset = wl_offset( ctx ) + 0x20;
    if( offset > ctx->size )
        return 0;
    
    offset = wl_offset( ctx ) + 0x1D;

    raw = *((time_t*)&ctx->b[offset-1]);
    raw <<= 8;

    return raw/1000.0;
}


int wl_get_gsm_signal( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    uint8_t level;

    offset = wl_offset( ctx ) + 0x27;
    if( offset > ctx->size )
        return -1;
    
    level = ctx->b[offset-1]&0x1F;
    return level;
}

int wl_get_software_version( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    uint16_t version;

    if( ctx->packet_type != 0x0E )
        return -1;
    
    offset = wl_offset( ctx  ) + 0x13;
    if( ctx->size < offset )
        return -1;

    version = *(uint16_t*)&ctx->b[offset-1];

    if( version )
    {
        ctx->fw_version = version;
    }

    return 0;
}

double wl_get_battery( struct t_packet_ctx *ctx )
{
    uint16_t raw_voltage;
    uint8_t bat_offset;
    uint8_t dev_type;

    double voltage;

    bat_offset = wl_offset( ctx  ) + 0x29;
    if( bat_offset > ctx->size )
        return 0;

    raw_voltage = ctx->b[bat_offset-1];


    bat_offset = wl_offset( ctx  ) + 0x2A;
    if( bat_offset > ctx->size )
        return 0;

    raw_voltage |= (ctx->b[bat_offset-1]&0x0F)<<8;

    /* type of device? */
    dev_type = wl_get_hw_model( ctx );
    
    if( dev_type == 0x0D || dev_type == 0x12 )
    {
        voltage = 0.0016 * raw_voltage;
    }
    else
    {
        voltage = 0.0049 * raw_voltage + 0.0235;
    }

    return voltage;
}

uint8_t wl_get_state( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x13;
    if( offset > ctx->size )
        return 0;

    return ctx->b[offset-1];
}

char *wl_get_state_s( struct t_packet_ctx *ctx )
{
    switch( wl_get_state( ctx ) )
    {
        case 0x00: return "No Reason Recorded";
        case 0x01: return "Response";
        case 0x02: return "Tracking";
        case 0x04: return "Event";
        case 0x06: return "Emergency";
        case 0x07: return "Main Power Low";
        case 0x08: return "Static pin IN";
        case 0x09: return "Static pin OUT";
        case 0x0A: return "Strong impact";
        case 0x0B: return "Mobile pin IN";
        case 0x0C: return "Mobile pin OUT";
        case 0x0D: return "Weak impact";
        case 0x0E: return "Location Update";
        case 0x16: return "Locked";
        case 0x17: return "Unlocked";
        case 0x18: return "Maintenance";
        case 0x19: return "Break in";
        case 0x1D: return "Power On";
        case 0x1E: return "Light Off";
        case 0x1F: return "Light On";
        case 0x20: return "Temperature Low";
        case 0x21: return "Temperature High";
        case 0x25: return "Jamming";
        case 0x26: return "Logging";
        default: return "Unknown (";
    }
}

double wl_get_longitude( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x0F;
    if( offset > ctx->size )
        return 0.0;

    offset = wl_offset( ctx  ) + 0x0B;

    return (*(uint32_t*)&ctx->b[offset-1])*0.00000057295779513082320876;
}

double wl_get_latitude( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x13;
    if( offset > ctx->size )
        return -1;

    offset = wl_offset( ctx  ) + 0x0F;

    return (*(uint32_t*)&ctx->b[offset-1])*0.00000057295779513082320876;
}

uint8_t wl_get_gps_speed( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    offset = wl_offset( ctx  ) + 0x1B;
    if( offset > ctx->size )
        return 0;
    
    return ctx->b[offset-1];
}

double wl_get_gps_angle( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    offset = wl_offset( ctx  ) + 0x1C;
    if( offset > ctx->size )
        return 0.0;

    return ((uint16_t)ctx->b[offset-1]*360)/255.0;
}

int16_t wl_get_gps_altitude( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    offset = wl_offset( ctx  ) + 0x21;
    if( offset > ctx->size )
        return 0;

    offset = wl_offset( ctx  ) + 0x20;
    
    return *(int16_t*)&ctx->b[offset-1]&0x7FFF;
}

time_t wl_get_gps_time( struct t_packet_ctx *ctx )
{
    uint8_t     offset;
    
    offset = wl_offset( ctx  ) + 0x22;

    return *((time_t*)&ctx->b[offset-1]);
}

uint8_t wl_get_gps_sat( struct t_packet_ctx *ctx )
{
    uint8_t     offset;
    
    offset = wl_offset( ctx  ) + 0x26;

    return ctx->b[offset-1]&0x07;
}

uint8_t wl_get_gps_stat( struct t_packet_ctx *ctx )
{
    uint8_t     offset;
    
    offset = wl_offset( ctx  ) + 0x26;

    return ctx->b[offset-1]>>3;
}

double wl_get_gps_hdop( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x3C;
    if( offset > ctx->size )
        return -1;
    
    return (((uint16_t)ctx->b[offset-1]*100)/30.0)/100.0;
}

uint16_t wl_get_gps_ttff( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x3A;
    if( offset > ctx->size )
        return 0;

    return *((uint16_t*)&ctx->b[offset-1]);
}

uint8_t wl_get_reason( struct t_packet_ctx *ctx )
{
    uint8_t     offset;
    
    offset = wl_offset( ctx  ) + 0x18;

    return ctx->b[offset-1];
}

char *wl_get_reason_s( struct t_packet_ctx *ctx )
{
    switch( wl_get_reason( ctx ) )
    {
        case 0x00: return "Unknown";
        case 0x01: return "Locked";
        case 0x02: return "Unlocked";
        case 0x03: return "Maintenance";
        case 0x04: return "Break in";
        default: return "Unknown";
    }
}

uint8_t wl_get_inputs_low( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    offset = wl_offset( ctx  ) + 0x16;
    if( offset > ctx->size )
        return 0;
    
    return ctx->b[offset-1];
}

uint32_t com_tout_array[] = 
{
	0x00000000, 0x0000000A, 0x0000000F, 0x00000014, 0x00000019, 0x0000001E, 0x00000028, 0x00000032,
	0x0000003C, 0x0000005A, 0x00000078, 0x00000096, 0x000000B4, 0x000000F0, 0x0000012C, 0x00000168,
	0x000001A4, 0x000001E0, 0x0000021C, 0x00000258, 0x00000384, 0x000004B0, 0x000005DC, 0x00000708,
	0x00000834, 0x00000960, 0x00000A8C, 0x00000BB8, 0x00000E10, 0x00001068, 0x000012C0, 0x00001518,
	0x00001770,	0x000019C8, 0x00001C20, 0x00002328, 0x00002A30, 0x00003138, 0x00003840, 0x00003F48,
	0x00004650, 0x00005460, 0x00006270, 0x00007080, 0x00007E90, 0x00008CA0, 0x0000A8C0, 0x0000C4E0,
	0x0000E100, 0x0000FD20, 0x00011940, 0x00015180, 0x0001FA40, 0x0002A300, 0x00034BC0, 0x0003F480,  
	0x00049D40, 0x00054600, 0x0005EEC0, 0x00069780
};

int wl_protocol_is_supported( struct t_packet_ctx *ctx )
{
    uint8_t pr = ctx->protocol_id;
    return (pr == 5) || (pr == 7) || (pr == 3) || (pr == 9);
}

int wl_get_timeout( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    uint32_t result;

    if( wl_protocol_is_supported( ctx ) == 0 )
        return 0;

    if( ctx->packet_type == 0x0E )
    {
        offset = wl_offset( ctx ) + 0x15;
    }
    else if( ctx->packet_type == 0x01 )
    {
        offset = wl_offset( ctx ) + 0x28;
    }
    else
    {
        offset = 0;
    }

    if( offset > ctx->size )
        return -1;
    
    result = 0;

    result = ctx->b[offset-1];
    
    /* read timeout */
    if( result <= 59 )
    {
        result = (1000u * com_tout_array[result]) + 300000u;
    }

    if( result && ctx->com_timeout != result )
    {
        ctx->com_timeout = result;
    }
    
    return result;     
}


int wl_init_packet_ctx( struct t_packet_ctx *ctx, uint8_t *b, uint8_t size )
{
    uint8_t model, cmd;
    
    
    ctx->b = b;
    ctx->size = size;
    ctx->fw_version = 0xFFFF;
    ctx->protocol_id = 0;

    if( size < 7 )
        return -1;

    if( calc_xor8( ctx->b, ctx->size  ) != 0 )
        return -2;

    if( ctx->b[0] != '$' )
        return -3;

    if( wl_crypted( ctx  ) )
    {
        prepare_packet( 1, ctx->b, ctx->b, ctx->size );
    }

    model =  wl_get_hw_model( ctx );
    /* invalid model ? */
    if( model < 0 || model > 0x15 )
        return -4;

    cmd = ctx->b[5]&0x1F;

    ctx->packet_type = 0;

    if( cmd == 4 ) /* EEPROM data ? */
    {
        switch( ctx->b[7] )
        {
            case 1:
                ctx->packet_type = 13;
            break;
            case 2:
                ctx->packet_type = 5;
            break;
            case 3:
                ctx->packet_type = 6;
            break;
            case 4:
                ctx->packet_type = 15;
            break;
            case 5:
                ctx->packet_type = 16;
            break;
        }
    }
    else
    {
        switch( cmd )
        {
            case 5:
                ctx->packet_type = 17;
                break;
            case 6:
                ctx->packet_type = 18;
                break;
            case 3:
                if ( ctx->b[5] & 0x80 )
                    ctx->packet_type = 9;
                else
                    ctx->packet_type = 2; /* get IMEI ? */
                break;
            default:
                if ( cmd )
                    ctx->packet_type = 1;  /*  Tracking? */
                else
                    ctx->packet_type = 14; /* PING? */
                break;
        }
    }
    
    /* invalid combination ? */
    if( ctx->packet_type == 0 )
        return -5;

    ctx->protocol_id = 0x09;

    return 0;
}

/* packet codec */
int prepare_packet( uint8_t decrypt, uint8_t *in, uint8_t *out, uint8_t size )
{
    int i;
    uint32_t *block;

    if( size < 7 )
        return -1;

    /* copy header */
    i = 6;
    while( i-- )
    {
        out[i] = in[i];
    }
    out+=6;

    for( i = 7; size >= (i+7); i+=8 )
    {
        block = (uint32_t *)out;

        block[0] = (in[i + 2] << 24) + (in[i + 1] << 16) + (in[i] << 8) + in[i - 1];
        block[1] = (in[i + 6] << 24) + (in[i + 5] << 16) + (in[i + 4] << 8) + in[i + 3];
        
        if( decrypt )
        {
            xxtea_decrypt_block( block, xxtea_key );
        }
        else
        {
            xxtea_encrypt_block( block, xxtea_key );
        }
        out+=8;
    }
    return size;
}

static uint8_t hex2val( char ch )
{
  return ( ch > '9') ?(ch & 0xDFu ) - 0x37u: (ch - '0');
}

static char val2hex[] = "0123456789ABCDEF";

int hexstring_to_bin( char *s, uint8_t *out, uint8_t size )
{
    uint8_t i, c, t;
    char ch;

    c = 0;
    t = 0;
    i = 0;
    while( *s )
    {
        ch = *s++;
        if( ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n' )
        {
            continue;
        }
            
        if( ch == 'x' )
        {
            c = 0;
            continue;
        }
    

        t <<= 4;
        t |= hex2val( ch );
        ++c;

        if( c == 2 )
        {
            if( i < size ) *out++ = t;
            ++i;
            c = 0;
            t = 0;
        }
    }

    return i;
}

int packet_create_ex( uint32_t serial, char *in )
{
    uint8_t t, c;
    uint8_t data[128];
    uint8_t len, blen, i;

    /* fill unused data with FFff */
    len = sizeof( data );
    while( len-- )
    {
        data[len] = 0xFF;
    }

    len = xstrlen( in );

    if( len < 1 )
        return -1;
    
    c = 0;
    t = 0;
    blen = 0;

    blen = hexstring_to_bin( in, data, 255 );
    if( blen < 6 )
        return -1;

    prepare_packet( 0, data, data, blen );

    data[blen] = calc_xor8( data, blen );
    /* add crc8 */
    blen += 1;

    *in++ = '<';
    for( i = 0; i < blen; i++ )
    {
        t = data[i];
        *in++ = val2hex[t>>4];
        *in++ = val2hex[t&0x0F];
    }
    *in++ = '\r';
    *in++ = '\n';
    *in = 0;

    return (blen*2)+3;
}

/* serial - device serial, in - hex string */
int packet_create( uint32_t serial, char *in )
{
    uint8_t t, c;
    uint8_t data[128];
    uint8_t len, blen, i;

    /* fill unused data with FFff */
    len = sizeof( data );
    while( len-- )
    {
        data[len] = 0xFF;
    }

    len = xstrlen( in );

    if( len < 1 )
        return -1;
    
    c = 0;
    t = 0;
    blen = 0;

    /* construct packet */
    /* header */
    data[0] = '$';
    
    /* serial */
    data[1] = (serial>>0)&0xFF;
    data[2] = (serial>>8)&0xFF;
    data[3] = (serial>>16)&0xFF;
    data[4] = (serial>>24)&0xFF;
    
    /* random data */
    data[5] = (rand()&0x7F) | 0x80u;

    
    blen = hexstring_to_bin( in, &data[6], 255 );

    /* add padding to 8bytes align */
    blen = (blen + (8-1))&(~(8-1));
    
    /* add header size */
    blen += 6;
    
    prepare_packet( 0, data, data, blen );

    data[blen] = calc_xor8( data, blen );
    /* add crc8 */
    blen += 1;

    /* return back data to hex string */
    *in++ = '\\';
    *in++ = 't';
    *in++ = 'c';
    for( i = 0; i < blen; i++ )
    {
        t = data[i];
        *in++ = val2hex[t>>4];
        *in++ = val2hex[t&0x0F];
    }
    *in++ = '\r';
    *in++ = '\n';
    *in = 0;

    return (blen*2)+5;
}

int voice_call_cmd( char *phone, char *out )
{
    uint8_t len, i, t;
    char *s = out;
    
    len = xstrlen( phone );
    if( !len || len > 14 )
        return -1;

    *s++ = '0';
    *s++ = '0';   
    *s++ = ' ';
    *s++ = '1';
    *s++ = '0';
    *s++ = ' ';
    *s++ = val2hex[len>>4];
    *s++ = val2hex[len&0x0F];

    for( i = 0; i < len; i++ )
    {
        t = phone[i];
        *s++ = ' ';
        *s++ = val2hex[t>>4];
        *s++ = val2hex[t&0x0F];
    }
    for( ; i < 14; i++ )
    {
        *s++ = ' ';
        *s++ = '0';
        *s++ = '0';
    }   
    *s = '\0';

    return xstrlen( out );
}

int packet_process( char *s )
{
    uint8_t data[128];
    char tmp[64];
    struct t_packet_ctx ctx = { 0 };
    uint8_t blen, i;
    int r;

    struct tm * ptm;
    time_t raw_time;


    blen = hexstring_to_bin( s, data, 255 ); 
    
    r = wl_init_packet_ctx( &ctx, data, blen );
    if( r < 0 )
        return -1;

    struct t_track_info *p;

    switch( ctx.packet_type )
    {
        case 0x01:
            p = (struct t_track_info *)ctx.b;

            print_control( SERIAL_CONTROL, "%.6d", wl_get_device_serial( &ctx ) );
            print_control( HARDWARE_CONTROL, wl_get_device_type_s( &ctx ) );

            print_control( MILEAGE_CONTROL, "%d", wl_get_gps_mileage( &ctx ) );      
            print_control( MODEM_SIGNAL_CONTROL, "%d", wl_get_gsm_signal( &ctx ) );      

            print_control( BATTERY_VOLTAGE, "%.2f", wl_get_battery( &ctx ) );
            print_control( STATE_CONTROL, wl_get_state_s( &ctx ) );
            
            print_control( LONG_CONTROL, "%f", wl_get_longitude( &ctx ) );
            print_control( LAT_CONTROL, "%f", wl_get_latitude( &ctx ) );

            print_control( SPEED_CONTROL, "%d", wl_get_gps_speed( &ctx ) );
            print_control( ANGLE_CONTROL, "%f", wl_get_gps_angle( &ctx ) );
            print_control( ALT_CONTROL, "%d", wl_get_gps_altitude( &ctx ) );

            print_control( SATS_CONTROL, "%d", wl_get_gps_sat( &ctx ) );

            print_control( GPS_VALID_CONTROL, "%s", wl_get_gps_stat( &ctx )&1?"Yes":"No" );
            print_control( GPS_POWER_CONTROL, "%s", wl_get_gps_stat( &ctx )&2?"On":"Off" );
        
            print_control( GPS_HDOP_CONTROL, "%.2f", wl_get_gps_hdop( &ctx ) );

            print_control( GPS_TTFF_CONTROL, "%d", wl_get_gps_ttff( &ctx ) );

            print_control( DEV_REASON_CONTROL, wl_get_reason_s( &ctx ) );


            print_control( INSTALL_CONTROL, wl_get_inputs_low( &ctx )&0x01?"Installed":"Not Installed" );
            print_control( STATIC_PIN_CONTROL, wl_get_inputs_low( &ctx )&0x02?"OUT":"IN" );
            print_control( MOBILE_PIN_CONTROL, wl_get_inputs_low( &ctx )&0x04?"OUT":"IN" );


            print_control( TIMEOUT_CONTROL, "%d", wl_get_timeout( &ctx ) );

            raw_time = wl_get_gps_time( &ctx );
            ptm = gmtime ( &raw_time );
            print_control( GPS_TIME_CONTROL, asctime( ptm ) );         

            raw_time = wl_get_unit_time( &ctx );
            ptm = gmtime ( &raw_time );
            print_control( UNIT_TIME_CONTROL, asctime( ptm ) );
        break;
        /* IMEI? */
        case 0x02:
            print_control( SERIAL_CONTROL, "%.6d", wl_get_device_serial( &ctx ) );
            print_control( HARDWARE_CONTROL, wl_get_device_type_s( &ctx ) );
            if( wl_get_modem_imei( &ctx, tmp ) )
            {
                print_control( IMEI_CONTROL, tmp );
            }  
        break;
        /* ping ? */
        case 0x0E:
            print_control( SERIAL_CONTROL, "%.6d", wl_get_device_serial( &ctx ) );
            print_control( HARDWARE_CONTROL, wl_get_device_type_s( &ctx ) );

            print_control( LONG_CONTROL, "%f", wl_get_longitude( &ctx ) );
            print_control( LAT_CONTROL, "%f", wl_get_latitude( &ctx ) );

            print_control( GPS_HW_TYPE, wl_get_gps_type_s( &ctx ) );
            
    
            print_control( TIMEOUT_CONTROL, "%d", wl_get_timeout( &ctx ) );

            raw_time = wl_get_unit_time( &ctx );
            ptm = gmtime ( &raw_time );
            print_control( UNIT_TIME_CONTROL, asctime( ptm ) );

            if( wl_get_software_version( &ctx ) == 0 )
            {
                print_control( FW_VER_CONTROL, "%.2d.%.2d.%.4d", 
                    ctx.fw_version&0x3F,
                    (ctx.fw_version>>6)&0x1F,
                    ((ctx.fw_version>>11)&0x0F)+2000u
                 );
            }
        break;
        default:
            return -1;
    }

    /* return data back */
    r = 0;

    for( i =0; i < blen; i++ )
    {
        *s++ = val2hex[data[i]>>4];
        *s++ = val2hex[data[i]&0x0F];
        *s++ = ' ';
        r=+3;
    }
    *s = 0;

    return r;
}

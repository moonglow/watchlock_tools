#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

struct t_tran_ctx
{
    HANDLE hCom;
    int ncom;
};


struct t_tran_ctx *trans_init( int ncom );
int trans_deinit( struct t_tran_ctx *ctx );
int trans_data_write_buffer( struct t_tran_ctx *ctx, uint8_t *b, uint8_t size );
int trans_data_read_buffer( struct t_tran_ctx *ctx, uint8_t *b, uint8_t size, uint32_t t );
int trans_data_inqueue( struct t_tran_ctx *ctx );

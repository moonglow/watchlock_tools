#include <stdio.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include "wl_parser.h"

static uint32_t xxtea_key[4] = 
{
    0x4A1CB24E,
    0x068EC892,
    0xD9589FB6,
    0xF4988430
};

#define MX  ( ((z >> 5 ^ z << 2) + ((z >> 3 ^ z << 4))) ^ ((sum ^ z) + (k[(p & 3) ^ e] ^ z)) )

static void xxtea_decrypt_block( uint32_t *v, uint32_t *k )
{
    uint32_t z, y = v[0], sum = 0, e, DELTA = 0x9e3779b9;
    uint32_t p, q;

    q = 32;
    sum = q * DELTA;
    while (sum != 0)
    {
        e = sum >> 2 & 3;
        
        p = 1;
        z = v[0];
        y = v[1] -= MX;

        p = 0;
        z = v[1];
        y = v[0] -= MX;
        sum -= DELTA;
    }   
}

static void xxtea_encrypt_block( uint32_t *v, uint32_t *k )
{
    uint32_t z, y = v[0], sum = 0, e, DELTA = 0x9e3779b9;
    uint32_t p, q;

    z = v[1];
    q = 32;
    while (q-- > 0)
    {
        sum += DELTA;
        e = sum >> 2 & 3;

        p = 0;
        y = v[1];
        z = v[0] += MX;

        p = 1;
        y = v[0];
        z = v[1] += MX;
    }
}

uint8_t calc_xor8( uint8_t *b, uint8_t size )
{
    uint8_t crc = 0;

    while( size-- )
    {
        crc ^= b[size];
    }
    return crc;
}

/* protocol parser */
static uint8_t wl_crypted( struct t_packet_ctx *ctx )
{
    /* data crypted ?*/
    if( (ctx->b[5] & 0x40) )
        return 1;
    return 0;
}

static uint8_t wl_offset( struct t_packet_ctx *ctx )
{
    /* need to shift offset ?*/
    if( (ctx->b[5] & 0x20) )
        return 1;
    return 0;
}


int wl_get_modem_imei( struct t_packet_ctx *ctx, char *out )
{
    uint8_t offset, len;
    offset = wl_offset( ctx ) + 0x07;
    if( offset >= ctx->size )
        return 0; 
   
    len = ctx->b[offset-1];
    
    offset = wl_offset( ctx ) + 0x08;
    
    out[len] = 0;
    while( len-- )
    {
        out[len] = ctx->b[offset-1+len];
    }
    return strlen( out );
}

uint32_t wl_get_device_serial( struct t_packet_ctx *ctx ) 
{
    return ( (ctx->b[4]<<24)|(ctx->b[3]<<16)|(ctx->b[2]<<8)|(ctx->b[1]<<0) );   
}


double wl_get_longitude( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x0F;
    if( offset > ctx->size )
        return 0.0;

    offset = wl_offset( ctx  ) + 0x0B;

    return (*(uint32_t*)&ctx->b[offset-1])*0.00000057295779513082320876;
}

double wl_get_latitude( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x13;
    if( offset > ctx->size )
        return -1;

    offset = wl_offset( ctx  ) + 0x0F;

    return (*(uint32_t*)&ctx->b[offset-1])*0.00000057295779513082320876;
}

/* get device hardware model */
uint8_t wl_get_hw_model( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx ) + 0x06;
    if( offset >= ctx->size )
        return 0;  
    
    return ctx->b[offset-1];
}

uint8_t wl_get_reason( struct t_packet_ctx *ctx )
{
    uint8_t     offset;
    
    offset = wl_offset( ctx  ) + 0x18;

    return ctx->b[offset-1];
}

uint8_t wl_get_state( struct t_packet_ctx *ctx )
{
    uint8_t offset;

    offset = wl_offset( ctx  ) + 0x13;
    if( offset > ctx->size )
        return 0;

    return ctx->b[offset-1];
}

char *wl_get_state_s( uint8_t status )
{
    switch( status )
    {
        case 0x00: return "No Reason Recorded";
        case 0x01: return "Response";
        case 0x02: return "Tracking";
        case 0x04: return "Event";
        case 0x06: return "Emergency";
        case 0x07: return "Main Power Low";
        case 0x08: return "Static pin IN";
        case 0x09: return "Static pin OUT";
        case 0x0A: return "Strong impact";
        case 0x0B: return "Mobile pin IN";
        case 0x0C: return "Mobile pin OUT";
        case 0x0D: return "Weak impact";
        case 0x0E: return "Location Update";
        case 0x16: return "Locked";
        case 0x17: return "Unlocked";
        case 0x18: return "Maintenance";
        case 0x19: return "Break in";
        case 0x1D: return "Power On";
        case 0x1E: return "Light Off";
        case 0x1F: return "Light On";
        case 0x20: return "Temperature Low";
        case 0x21: return "Temperature High";
        case 0x25: return "Jamming";
        case 0x26: return "Logging";
        default: return "Unknown (";
    }
}

char *wl_get_reason_s( uint8_t reason )
{
    switch( reason )
    {
        case 0x00: return "Unknown";
        case 0x01: return "Locked";
        case 0x02: return "Unlocked";
        case 0x03: return "Maintenance";
        case 0x04: return "Break in";
        default: return "Unknown";
    }
}

char *wl_get_device_type_s( uint8_t hw_ver ) 
{
    switch( hw_ver )
    {
        case 0x01: return "Rainbow";
        case 0x02: return "Helios Mot24";
        case 0x03: return "Helios MotUSB";       
        case 0x04: return "Helios Enf24";        
        case 0x05: return "Helios EnfUSB";     
        case 0x06: return "Triton Enf";  
        case 0x07: return "Watchlock Mot";      
        case 0x08: return "Triton Mot";
        case 0x09: return "Helios Tel24";
        case 0x0A: return "Helios Tel10";       
        case 0x0B: return "Helios uBx24";        
        case 0x0C: return "Helios uBx10";        
        case 0x0D: return "Watchlock uBx";        
        case 0x0E: return "Triton uBx";       
        case 0x0F: return "Rainbow uBx";       
        case 0x10: return "Helios Mot24b";      
        case 0x11: return "Helios Mot10";      
        case 0x12: return "Kylos 2 Tel";     
        case 0x13: return "Triton 2 Tel";     
        case 0x14: return "Helios TTa";     
        case 0x15: return "Helios TT";   
        default: return "Unknown (";
    }
}

int wl_get_software_version( struct t_packet_ctx *ctx )
{
    uint8_t offset;
    uint16_t version;

    if( ctx->packet_type != 0x0E )
        return -1;
    
    offset = wl_offset( ctx  ) + 0x13;
    if( ctx->size < offset )
        return -1;

    version = *(uint16_t*)&ctx->b[offset-1];

    if( version )
    {
        ctx->fw_version = version;
    }

    return 0;
}

double wl_get_battery( struct t_packet_ctx *ctx )
{
    uint16_t raw_voltage;
    uint8_t bat_offset;
    uint8_t dev_type;

    double voltage;

    bat_offset = wl_offset( ctx  ) + 0x29;
    if( bat_offset > ctx->size )
        return 0;

    raw_voltage = ctx->b[bat_offset-1];


    bat_offset = wl_offset( ctx  ) + 0x2A;
    if( bat_offset > ctx->size )
        return 0;

    raw_voltage |= (ctx->b[bat_offset-1]&0x0F)<<8;

    /* type of device? */
    dev_type = wl_get_hw_model( ctx );
    
    if( dev_type == 0x0D || dev_type == 0x12 )
    {
        voltage = 0.0016 * raw_voltage;
    }
    else
    {
        voltage = 0.0049 * raw_voltage + 0.0235;
    }

    return voltage;
}


/* packet codec */
int prepare_packet( uint8_t decrypt, uint8_t *in, uint8_t *out, uint8_t size )
{
    int i;
    uint32_t *block;

    if( size < 7 )
        return -1;

    /* copy header */
    i = 6;
    while( i-- )
    {
        out[i] = in[i];
    }
    out+=6;

    for( i = 7; size >= (i+7); i+=8 )
    {
        block = (uint32_t *)out;

        block[0] = (in[i + 2] << 24) + (in[i + 1] << 16) + (in[i] << 8) + in[i - 1];
        block[1] = (in[i + 6] << 24) + (in[i + 5] << 16) + (in[i + 4] << 8) + in[i + 3];
        
        if( decrypt )
        {
            xxtea_decrypt_block( block, xxtea_key );
        }
        else
        {
            xxtea_encrypt_block( block, xxtea_key );
        }
        out+=8;
    }
    return size;
}

int wl_init_packet_ctx( struct t_packet_ctx *ctx, uint8_t *b, uint8_t size )
{
    uint8_t model, cmd;
    
    
    ctx->b = b;
    ctx->size = size;
    ctx->fw_version = 0xFFFF;
    ctx->protocol_id = 0;

    if( size < 7 )
        return -1;

    if( calc_xor8( ctx->b, ctx->size  ) != 0 )
        return -2;

    if( ctx->b[0] != '$' )
        return -3;

    if( wl_crypted( ctx  ) )
    {
        prepare_packet( 1, ctx->b, ctx->b, ctx->size );
    }

    model =  wl_get_hw_model( ctx );
    /* invalid model ? */
    if( model < 0 || model > 0x15 )
        return -4;

    cmd = ctx->b[5]&0x1F;

    ctx->packet_type = 0;

    if( cmd == 4 ) /* EEPROM data ? */
    {
        switch( ctx->b[7] )
        {
            case 1:
                ctx->packet_type = 13;
            break;
            case 2:
                ctx->packet_type = 5;
            break;
            case 3:
                ctx->packet_type = 6;
            break;
            case 4:
                ctx->packet_type = 15;
            break;
            case 5:
                ctx->packet_type = 16;
            break;
        }
    }
    else
    {
        switch( cmd )
        {
            case 5:
                ctx->packet_type = 17;
                break;
            case 6:
                ctx->packet_type = 18;
                break;
            case 3:
                if ( ctx->b[5] & 0x80 )
                    ctx->packet_type = 9;
                else
                    ctx->packet_type = 2; /* got IMEI ? */
                break;
            default:
                if ( cmd )
                    ctx->packet_type = 1;  /*  Tracking? */
                else
                    ctx->packet_type = 14; /* PING? */
                break;
        }
    }
    
    /* invalid combination ? */
    if( ctx->packet_type == 0 )
        return -5;

    ctx->protocol_id = 0x09;

    return 0;
}


#pragma once


#include <time.h>

#pragma pack(push,1)
struct t_command
{
    char        start;
    uint32_t    serial;
    uint8_t     rnd;
    uint8_t     data[8];
    uint8_t     xor8;
};
#pragma pack(pop)


struct t_packet_ctx
{
    uint8_t     *b;
    uint8_t     size;
    uint8_t     protocol_id;
    uint8_t     packet_type;
    uint32_t    com_timeout;
    uint16_t    fw_version;
};

int prepare_packet( uint8_t decrypt, uint8_t *in, uint8_t *out, uint8_t size );
uint8_t calc_xor8( uint8_t *b, uint8_t size );

int wl_init_packet_ctx( struct t_packet_ctx *ctx, uint8_t *b, uint8_t size );

int wl_get_modem_imei( struct t_packet_ctx *ctx, char *out );
uint32_t wl_get_device_serial( struct t_packet_ctx *ctx );
char *wl_get_device_type_s( uint8_t hw_ver );
char *wl_get_reason_s( uint8_t reason );
char *wl_get_state_s( uint8_t status );
double wl_get_longitude( struct t_packet_ctx *ctx );
double wl_get_latitude( struct t_packet_ctx *ctx );
uint8_t wl_get_hw_model( struct t_packet_ctx *ctx );
double wl_get_battery( struct t_packet_ctx *ctx );
uint8_t wl_get_reason( struct t_packet_ctx *ctx );
uint8_t wl_get_state( struct t_packet_ctx *ctx );
int wl_get_software_version( struct t_packet_ctx *ctx );

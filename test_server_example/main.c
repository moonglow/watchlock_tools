#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>

#include "wl_parser.h"

#pragma comment(lib, "ws2_32.lib")

#define CON_UNKNOWN 	0
#define CON_DEVICE 		1
#define CON_WEB 		2


static const char http_resp[] = "HTTP/1.1 200 Ok\nContent-Type: text/html\nConnection: close\n\n";
static const char html_header_1[] ="<HTML><HEAD>\
<TITLE>BlindLock Server Status</TITLE>\
<meta http-equiv=\"refresh\" content=\"5\" >\
<style>\
table{width:100%;}\
table, th, td {border: 1px solid black;border-collapse: collapse;}\
th, td {padding: 5px;text-align: left;}\
table#t01 tr:nth-child(even) {background-color: #CCE6FF;}\
table#t01 tr:nth-child(odd) {background-color: #CCFFFF;}\
table#t01 th	{background-color: #E6FFCC;color: black;}\
</style>\
</HEAD>\
<BODY BGCOLOR=\"FFFFCC\">\
<HR>\
<H1>Active sessions: ";

static const char html_header_2[] = "</H1><HR>\
<table id=\"t01\">\
  <tr>\
    <th>Hardware</th>\
    <th>Device</th>\
    <th>Position</th>\
	<th>Voltage</th>\
    <th>Reason</th>\
    <th>Satus</th>\
    <th>Firmware</th>\
    <th>IMEI</th>\
  </tr>";

static const char html_footer[] =
"</table>\
</BODY>\
</HTML>";

static const char html_table_entry[] = 
 "<tr>\
    <td>%s</td>\
    <td>%.6d</td>\
    <td>%f %f</td>\
	<td>%.2f</td>\
    <td>%s</td>\
    <td>%s</td>\
    <td>%s</td>\
    <td>%s</td>\
  </tr>";


struct t_client
{
	int 		used;
	SOCKET 		sck;
	uint32_t	device;
	uint16_t	fw_ver;
    uint8_t     hw_ver;
	double 		lat;
	double 		lon;
	double 		battery;
	uint8_t 	reason;
    uint8_t     status;
	uint8_t 	con_state;
    char        imei[18];
};

static uint32_t g_clients = 0;
static struct t_client g_client_list[FD_SETSIZE] = { 0 };

struct t_client *find_client( SOCKET sck )
{
	int i = FD_SETSIZE;
	while( i-- )
	{
		if( g_client_list[i].used && g_client_list[i].sck == sck )
		{
			return &g_client_list[i];
		}	
	}
	return 0;
}

int add_client( SOCKET sck )
{
	int i = FD_SETSIZE;
	while( i-- )
	{
		if( !g_client_list[i].used )
		{
			memset( &g_client_list[i], 0x00, sizeof( struct t_client ) );
			g_client_list[i].used = 1;
			g_client_list[i].sck = sck;
			return i;
		}
	}
	return i;
}

int del_client( SOCKET sck )
{
	int i = FD_SETSIZE;
	while( i-- )
	{
		if( g_client_list[i].used && g_client_list[i].sck == sck )
		{
			g_client_list[i].used = 0;
			return i;
		}	
	}
	return i;
}

SOCKET create_server_socket( char *ip, uint16_t port )
{
    SOCKET sck;
    struct sockaddr_in addr;
    int result;

    sck = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
    if( sck == INVALID_SOCKET )
    {
        return INVALID_SOCKET;
    }

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr( ip );
    addr.sin_port = htons( port );
    
    result = bind( sck, (SOCKADDR *)&addr, sizeof (addr) );
    if ( result != 0 ) 
    {
        closesocket( sck );
        return INVALID_SOCKET;
    }

    return sck;
}

int process_web_request( struct t_client *client, uint8_t *data, int size )
{
	int result, i;

	if( client == 0 )
		return -1;
	if( strncmp( (char*)data, "GET", 3 ) != 0 )
		return -2;

	client->con_state = CON_WEB;

	send( client->sck, http_resp, sizeof( http_resp ), 0 );
	send( client->sck, html_header_1, sizeof( html_header_1 ), 0 );

	result = sprintf( (void*)data, "%d", g_clients );
	send( client->sck, (void*)data, result, 0 );
	send( client->sck, html_header_2, sizeof( html_header_2 ), 0 );

	i = FD_SETSIZE;
	while( i-- )
	{
        char tmp[16];
        uint16_t fw = g_client_list[i].fw_ver;
        if( fw )
        {
            sprintf( tmp, "%.2d.%.2d.%.4d", 
                    fw&0x3F,
                    (fw>>6)&0x1F,
                    ((fw>>11)&0x0F)+2000u );
        }
		if( !g_client_list[i].used )
			continue;
		
		if( g_client_list[i].con_state != CON_DEVICE )
			continue;
		
		result = sprintf( (void*)data, html_table_entry, 
            wl_get_device_type_s( g_client_list[i].hw_ver ),
			g_client_list[i].device,
			g_client_list[i].lat, g_client_list[i].lon,
			g_client_list[i].battery,
			wl_get_reason_s( g_client_list[i].reason ),
            wl_get_state_s( g_client_list[i].status ),
            fw ? tmp:"Unknown",
            g_client_list[i].imei[0] ? g_client_list[i].imei : "Unknown"
		 );
		send( client->sck, (void*)data, result, 0 );
	}


	send( client->sck, html_footer, sizeof( html_footer ), 0 );
	return -2;
}

int process_incoming_data( struct t_client *client, uint8_t *data, int size )
{
    int result;
    struct t_packet_ctx pctx = { 0 };

	if( client == 0 )
		return -1;

    result = wl_init_packet_ctx( &pctx, data, size );
    if( result != 0 )
        return 0;
    
    /* imei response */
    if( pctx.packet_type == 0x02 )
    {
        if( wl_get_modem_imei( &pctx, client->imei ) )
        {
            printf( "Device IMEI %s\r\n", client->imei );
        }
        return 0;
    }


    if( pctx.packet_type != 0x01 && pctx.packet_type != 0x0E )
        return 0;

	client->device = wl_get_device_serial( &pctx );
	client->lon = wl_get_longitude( &pctx );
	client->lat = wl_get_latitude( &pctx );
    client->hw_ver = wl_get_hw_model( &pctx );


    if( pctx.packet_type == 0x01 )
    {
        client->reason = wl_get_reason( &pctx );
        client->status = wl_get_state( &pctx );
		client->battery = wl_get_battery( &pctx );
        
        if( client->imei[0] == 0 )
        {
            /* try to request imei */
            struct t_command request = { 0 };
            request.start = '$';
            request.serial = client->device;
            request.rnd = (rand()&0x7F) | 0x80;
            memset( request.data, 0xFF, sizeof( request.data ) );
            
            request.data[0] = 0x1D;
            prepare_packet( 0, (void*)&request, (void*)&request, sizeof( request ) );
            request.xor8 = calc_xor8( (void*)&request, sizeof( request )-1 );

            result = send( client->sck, (void*)&request, sizeof( request ), 0 ); 
            printf( "[%.4X]request = %d\r\n", client->sck, result );
        }
        printf( "[%.4X][%.6d]Tracking\r\n", client->sck, client->device ); 
        /* update only on track information */
        client->con_state = CON_DEVICE;
    }
    else if( pctx.packet_type == 0x0E ) /* ping ? */
    {
        if( wl_get_software_version( &pctx ) == 0 )
        {
            client->fw_ver = pctx.fw_version;
        }
        printf( "[%.4X][%.6d]Ping\r\n", client->sck, client->device ); 
    }

    
    return 0;    
}


int socket_server( void )
{
    int result;
    struct sockaddr_in addr;
    SOCKET sck_server;
    fd_set fd_clients, fd_rd;
    char rxData[0x400];
    
    sck_server = create_server_socket( "0.0.0.0", 6600 );
    
    if( listen( sck_server, FD_SETSIZE ) != 0 )
        return -1;

    FD_ZERO( &fd_clients );
    FD_SET ( sck_server, &fd_clients );
    
    printf( "listen for clients...\r\n" );

    for( ;; )
    {
        int total = 0;
        fd_rd = fd_clients;

        total = select( FD_SETSIZE, &fd_rd, 0, 0, 0);
        if ( total == SOCKET_ERROR )
        {
            printf( "select fatal!\r\n" );
            return -2;
        }

        for ( int i = 0; i < total; ++i)
        {
            SOCKET sck_c = fd_rd.fd_array[i];

            if ( !FD_ISSET( sck_c, &fd_rd ) )
                continue;

            if( sck_c == sck_server )
            {
                int addr_len = sizeof( addr );
                SOCKET client;
                client = accept( sck_server, (SOCKADDR *)&addr, &addr_len );
                if( client == INVALID_SOCKET )
                {
                    printf( "accept fail!! fatal\r\n" );
                    return -3;
                }
                printf( "[%.4X]New client from %s:%d\r\n" , client, inet_ntoa (addr.sin_addr), ntohs (addr.sin_port) );
    			result = add_client( client );
				if( result < 0 )
				{
					printf( "add_client() failed\r\n" );
				}            
				FD_SET ( client, &fd_clients);
				++g_clients;
            }
            else
            {
                u_long adata = 0;

                /* process clients */
                result = ioctlsocket( sck_c, FIONREAD, &adata );
                if( result == 0 && adata )
                {
                    if( adata > sizeof( rxData ) )
                        adata = sizeof( rxData );

                    result = recv( sck_c, rxData, adata, 0 );

					if( rxData[0] == '$' )
					{
                    	result = process_incoming_data( find_client( sck_c ), (uint8_t*)rxData, adata );
					}
					else if( rxData[0] == 'G' )
					{
						result = process_web_request( find_client( sck_c ), (uint8_t*)rxData, adata );
					} 
					else
					{
						printf( "bad request\r\n" );
						result = -1;
					}
                }

                if( result < 0 || adata == 0 )
                {
	    			result = del_client( sck_c );
					if( result < 0 )
					{
						printf( "del_client() failed\r\n" );
					}
                    shutdown( sck_c, SD_BOTH );
                    closesocket( sck_c );
                    FD_CLR( sck_c, &fd_clients );
					--g_clients;
                    printf( "[%.4X]Closed\r\n", sck_c );
                }
            }

        }
    }

    return 0;
}

int main(int argc, char *argv[])
{
    int result;
    WSADATA wsaData;

    printf("Test server\r\n");
    
    result = WSAStartup( MAKEWORD(2,2), &wsaData );

    if ( result != 0)
    {
        printf( "wsock init failed = %d\r\n", result );
        return -1;
    }
    
    result = socket_server();

    WSACleanup();
    return 0;
}

